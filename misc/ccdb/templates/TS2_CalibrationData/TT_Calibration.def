###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: XX                        					            ##
##  CCDB device types: ICS_xxxxx                                                            ##
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: XX 								##
##                                                                                          ##
##                                 Sensor Calibration                                       ##
##                                                                                          ##
##                                                                                          ##
############################           Version: 1.1           ################################
# Author:	Emilio Asensi
# Date:		02-06-2020
# Version:  v1.1
# Changes:  Added parameters feedback and time stamp
############################           Version: 1.0           ################################
# Author:	Emilio Asensi
# Date:		01-06-2020
# Version:  v1.0
# Changes:  First version
##############################################################################################


############################
#  STATUS BLOCK
############################
define_status_block()

add_digital("CalibrationOK",             PV_DESC="Sensor calibrated",       PV_ONAM="True",                          PV_ZNAM="False")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking",                PV_ZNAM="AllowLocking")
add_digital("GroupAlarm",              PV_DESC="Group Alarm for OPI")
add_digital("DevLocked",               PV_DESC="Device locked",             PV_ONAM="True",                          PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                              PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                              PV_DESC="Guest Lock ID")
add_string("MeasValue_Msg",    39,     PV_NAME="Measured_Value_Msg",   	 PV_DESC="Measured Value Message")
add_string("UpdateTimeStamp",    39,     PV_NAME="UpdateTimeStamp",   	 PV_DESC="Feedback Update Time Stamp")

#Alarm signals
add_major_alarm("HIHI",            "LIMIT HIGH",                          PV_ZNAM="True")
add_major_alarm("LOLO",            "LIMIT LOW",                          PV_ZNAM="True")
add_major_alarm("IO_Error",            "HW IO Error",                       PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",             PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",            PV_ZNAM="NominalState")
add_major_alarm("Discrepancy_Error",   "Discrepancy_Error",                 PV_ZNAM="NominalState",                  PV_ONAM="Timeout")

#Feedback from parameters
add_string("FB_SerialNumber", 39,   PV_NAME="FB_SerialNumber",             PV_DESC="Feedback Sensor Serial Number")
add_analog("FB_PlugNumber",         "INT",                              PV_DESC="Feedback Plug Number")
add_analog("FB_ScaleLOW",           "REAL",                             PV_DESC="Feedback Scale LOW",  PV_EGU="K")
add_analog("FB_ScaleHIGH",          "REAL",                             PV_DESC="Feedback Scale HIGH", PV_EGU="K")

# Feedback from Curve0
add_analog("FB_Curve0_Length",      "INT",                              PV_DESC="Feedback Curve 0 Length")
add_analog("FB_ZL_0",               "REAL",                             PV_DESC="Feedback Curve 0 Limit LOW")
add_analog("FB_ZU_0",               "REAL",                             PV_DESC="Feedback Curve 0 Limit HIGH")

add_analog("FB_Curve0_0",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_1",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_2",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_3",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_4",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_5",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_6",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_7",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_8",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")
add_analog("FB_Curve0_9",           "REAL",                             PV_DESC="Feedback Curve 0 coefficient")

# Feedback Curve1
add_analog("FB_Curve1_Length",      "INT",                              PV_DESC="Feedback Curve 1 Length")
add_analog("FB_ZL_1",               "REAL",                             PV_DESC="Feedback Curve 1 Limit LOW")
add_analog("FB_ZU_1",               "REAL",                             PV_DESC="Feedback Curve 1 Limit HIGH")

add_analog("FB_Curve1_0",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_1",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_2",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_3",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_4",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_5",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_6",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_7",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_8",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")
add_analog("FB_Curve1_9",           "REAL",                             PV_DESC="Feedback Curve 1 coefficient")

# Feedback Curve2
add_analog("FB_Curve2_Length",      "INT",                              PV_DESC="Feedback Curve 2 Length")
add_analog("FB_ZL_2",               "REAL",                             PV_DESC="Feedback Curve 2 Limit LOW")
add_analog("FB_ZU_2",               "REAL",                             PV_DESC="Feedback Curve 2 Limit HIGH")

add_analog("FB_Curve2_0",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_1",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_2",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_3",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_4",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_5",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_6",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_7",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_8",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")
add_analog("FB_Curve2_9",           "REAL",                             PV_DESC="Feedback Curve 2 coefficient")




############################
#  COMMAND BLOCK
############################
define_command_block()

add_digital("CMD_Download",              PV_DESC="CMD: Download Curve")
add_digital("CMD_Update",                PV_DESC="CMD: Update Curve")
add_digital("CMD_InhibitCal",                PV_DESC="CMD: Inhibit Calibration")
add_digital("CMD_UnlockCal",                PV_DESC="CMD: Unlock Calibration")


############################
#  PARAMETER BLOCK
############################
define_parameter_block()

add_string("SerialNumber", 20,   PV_NAME="SerialNumber",             PV_DESC="Sensor Serial Number")
add_analog("PlugNumber",         "INT",                              PV_DESC="Plug Number")
add_analog("ScaleLOW",           "REAL",                             PV_DESC="Scale LOW",  PV_EGU="K")
add_analog("ScaleHIGH",          "REAL",                             PV_DESC="Scale HIGH", PV_EGU="K")

# Curve0
add_analog("Curve0_Length",      "INT",                              PV_DESC="Curve 0 Length")
add_analog("ZL_0",               "REAL",                             PV_DESC="Curve 0 Limit LOW")
add_analog("ZU_0",               "REAL",                             PV_DESC="Curve 0 Limit HIGH")

add_analog("Curve0_0",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_1",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_2",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_3",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_4",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_5",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_6",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_7",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_8",           "REAL",                             PV_DESC="Curve 0 coefficient")
add_analog("Curve0_9",           "REAL",                             PV_DESC="Curve 0 coefficient")

# Curve1
add_analog("Curve1_Length",      "INT",                              PV_DESC="Curve 1 Length")
add_analog("ZL_1",               "REAL",                             PV_DESC="Curve 1 Limit LOW")
add_analog("ZU_1",               "REAL",                             PV_DESC="Curve 1 Limit HIGH")

add_analog("Curve1_0",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_1",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_2",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_3",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_4",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_5",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_6",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_7",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_8",           "REAL",                             PV_DESC="Curve 1 coefficient")
add_analog("Curve1_9",           "REAL",                             PV_DESC="Curve 1 coefficient")

# Curve2
add_analog("Curve2_Length",      "INT",                              PV_DESC="Curve 2 Length")
add_analog("ZL_2",               "REAL",                             PV_DESC="Curve 2 Limit LOW")
add_analog("ZU_2",               "REAL",                             PV_DESC="Curve 2 Limit HIGH")

add_analog("Curve2_0",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_1",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_2",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_3",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_4",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_5",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_6",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_7",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_8",           "REAL",                             PV_DESC="Curve 2 coefficient")
add_analog("Curve2_9",           "REAL",                             PV_DESC="Curve 2 coefficient")

 #Time Stamp
add_string("P_UpdateTimeStamp",    39,     PV_NAME="P_UpdateTimeStamp",   	 PV_DESC="Update Time Stamp")

#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                              PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                              PV_DESC="Device ID after Blockicon Open")
