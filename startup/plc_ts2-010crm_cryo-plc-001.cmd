
# @field IPADDR
# @runtime YES
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field REQUIRE_plc_ts2-010crm_cryo-plc-001_VERSION
# @runtime YES

# @field S7_PORT
# @runtime YES
# Can override S7 port with this

# @field MB_PORT
# @runtime YES
# Can override Modbus port with this

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_plc_ts2-010crm_cryo-plc-001_PATH
# @runtime YES

#- S7 port           : 2000
#- Input block size  : 26938 bytes
#- Output block size : 0 bytes
#- Endianness        : BigEndian
s7plcConfigure("TS2-010CRM:Cryo-PLC-001", $(IPADDR=ics-ts2cryo-plc-01.tn.esss.lu.se), $(S7_PORT=2000), 26938, 0, 1, $(RECVTIMEOUT=300), 0)

#- Modbus port       : 502
drvAsynIPPortConfigure("TS2-010CRM:Cryo-PLC-001", $(IPADDR=ics-ts2cryo-plc-01.tn.esss.lu.se):$(MB_PORT=502), 0, 0, 1)

#- Link type         : TCP/IP (0)
#- The timeout is initialized to the (modbus) default if not specified
modbusInterposeConfig("TS2-010CRM:Cryo-PLC-001", 0, $(RECVTIMEOUT=0), 0)

#- Slave address     : 0
#- Function code     : 16 - Write Multiple Registers
#- Addressing        : Absolute (-1)
#- Data segment      : 20 words
drvModbusAsynConfigure("TS2-010CRM:Cryo-PLC-001write", "TS2-010CRM:Cryo-PLC-001", 0, 16, -1, 20, 0, 0, "S7-1500")

#- Slave address     : 0
#- Function code     : 3 - Read Multiple Registers
#- Addressing        : Relative (0)
#- Data segment      : 10 words
#- Polling           : 1000 msec
drvModbusAsynConfigure("TS2-010CRM:Cryo-PLC-001read", "TS2-010CRM:Cryo-PLC-001", 0, 3, 0, 10, 0, 1000, "S7-1500")

#- Load plc interface database
dbLoadRecords("plc_ts2-010crm_cryo-plc-001.db", "PLCNAME=TS2-010CRM:Cryo-PLC-001, MODVERSION=$(REQUIRE_plc_ts2-010crm_cryo-plc-001_VERSION), S7_PORT=$(S7_PORT=2000), MODBUS_PORT=$(MB_PORT=502), PAYLOAD_SIZE=26938")

#- Configure autosave
#- Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

#- Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_plc_ts2-010crm_cryo-plc-001_PATH)", "misc")

#- Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

#- Specify what save files should be restored
set_pass0_restoreFile("plc_ts2-010crm_cryo-plc-001.sav")

#- Create monitor set
doAfterIocInit("create_monitor_set('plc_ts2-010crm_cryo-plc-001.req', 1, '')")
